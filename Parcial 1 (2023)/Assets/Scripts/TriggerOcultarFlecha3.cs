using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerOcultarFlecha3 : MonoBehaviour
{
    public GameObject arrow4;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            arrow4.SetActive(false);
        }
    }
}
