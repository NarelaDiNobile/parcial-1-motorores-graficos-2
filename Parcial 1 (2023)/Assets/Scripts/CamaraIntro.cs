using UnityEngine;
using System.Collections;

public class CamaraIntro : MonoBehaviour
{
    public Animator animator;
    public Camera mainCamera;

    void Start()
    {
        // Desactivar la c�mara principal al inicio de la escena
        mainCamera.enabled = false;

        // Reproducir la animaci�n de la c�mara al inicio de la escena
        StartCoroutine(PlayCameraAnimation());
    }

    private IEnumerator PlayCameraAnimation()
    {
        // Reproducir la animaci�n de la c�mara
        animator.SetTrigger("PlayAnimation");

        // Esperar hasta que la animaci�n haya terminado
        yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length);

        // Activar la c�mara principal y desactivar la c�mara de la animaci�n
        mainCamera.enabled = true;
        gameObject.SetActive(false);
    }
}
