using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager8 : MonoBehaviour
{
    public GameObject dialoguePanel8;
    public TMP_Text dialogueText8;

    private bool isInRange8 = false;
    private bool hasTriggeredDialogue8 = false;

    void Start()
    {
        dialoguePanel8.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange8 = true;
            dialoguePanel8.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange8 = false;
            dialoguePanel8.SetActive(false);
        }
    }

    void Update()
    {
        if (isInRange8 && Input.GetKeyDown(KeyCode.Space) && !hasTriggeredDialogue8)
        {
            StartDialogue();
            hasTriggeredDialogue8 = true;
        }
    }

    void StartDialogue()
    {
        // Aqu� puedes implementar tu l�gica de di�logo.
        // Puedes usar un sistema de di�logo existente o crear uno personalizado.

        // Ejemplo b�sico:
        dialogueText8.text = "�Hola! Soy el NPC. �Bienvenido!";
    }
}