using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerHueso2 : MonoBehaviour
{
    public Animator Hueso2;


    private void OnTriggerEnter(Collider other)
    {
        Hueso2.Play("AnimarHueso2");
    }


    private void OnTriggerExit(Collider other)
    {
        Hueso2.Play("AnimarHueso2");
    }
}
