using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerCofre : MonoBehaviour
{
    public Animator Cofre;

    private void OnTriggerEnter(Collider other)
    {
        Cofre.Play("AnimarAbrirCofre");
    }

    private void OnTriggerExit(Collider other)
    {
        Cofre.Play("AnimarCerrarCofre");
    }
}
