using UnityEngine;

public class ActivarFlecha : MonoBehaviour
{
    public GameObject objectToShowHide;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            objectToShowHide.SetActive(true);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            objectToShowHide.SetActive(false);
        }
    }
}
