using UnityEngine;

public class SlowPowerUp : MonoBehaviour
{
    public float slowdownFactor = 0.5f; // Factor de reducci�n de velocidad

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Obstacle")) // Comprueba si el jugador colisiona con el objeto deseado (etiquetado como "Obstacle")
        {
            Rigidbody playerRigidbody = GetComponent<Rigidbody>();

            if (playerRigidbody != null)
            {
                playerRigidbody.velocity *= slowdownFactor; // Reduce la velocidad del jugador por el factor especificado
            }
        }
    }
}


