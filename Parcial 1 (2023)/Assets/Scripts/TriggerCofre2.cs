using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerCofre2 : MonoBehaviour
{
    public Animator Cofre2;

    private void OnTriggerEnter(Collider other)
    {
        Cofre2.Play("AnimarAbrirCofre2");
    }

    private void OnTriggerExit(Collider other)
    {
        Cofre2.Play("AnimarCerrarCofre2");
    }
}
