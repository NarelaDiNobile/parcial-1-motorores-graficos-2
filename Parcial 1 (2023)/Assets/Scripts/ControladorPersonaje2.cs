using UnityEngine;

public class ControladorPersonaje2 : MonoBehaviour
{
    public PowerUpManager powerUpManager;
    public float normalSpeed = 10f;
    public float powerUpSpeed = 15f;
    public float fastDuration = 20f; // Duraci�n de la velocidad r�pida en segundos
    public float slowDuration = 2f; // Duraci�n de la velocidad lenta en segundos

    private Rigidbody rb2;
    private float currentSpeed; // Velocidad actual del jugador
    private float speedTimer; // Temporizador para controlar la duraci�n de la velocidad actual

    private void Start()
    {
        rb2 = GetComponent<Rigidbody>();
        currentSpeed = normalSpeed; // Inicialmente, la velocidad del jugador es la velocidad normal
    }

    private void FixedUpdate()
    {
        if (powerUpManager.isPoweredUp)
        {
            speedTimer += Time.deltaTime;

            if (currentSpeed != powerUpSpeed && speedTimer < fastDuration)
            {
                currentSpeed = powerUpSpeed;
            }
            else if (currentSpeed != normalSpeed && speedTimer >= fastDuration && speedTimer < (fastDuration + slowDuration))
            {
                currentSpeed = normalSpeed;
            }
            else if (speedTimer >= (fastDuration + slowDuration))
            {
                currentSpeed = normalSpeed;
                speedTimer = 0f;
                powerUpManager.isPoweredUp = false; // Desactiva el power-up despu�s de la duraci�n total
            }
        }

        rb2.velocity = transform.forward * currentSpeed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Hueso"))
        {
            powerUpManager.isPoweredUp = true;
            speedTimer = 0f; // Reinicia el temporizador al activar el power-up
            Destroy(other.gameObject);
        }
    }
}
