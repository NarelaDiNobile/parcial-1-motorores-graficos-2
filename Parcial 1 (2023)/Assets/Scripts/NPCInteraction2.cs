using UnityEngine;
using UnityEngine.UI;

public class NPCInteraction2 : MonoBehaviour
{
    public GameObject panel2;
    public Text interactText2;
    public KeyCode interactKey2 = KeyCode.E;
    public GameObject dialogBox2;
    private bool isInRange2 = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange2 = true;
            panel2.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange2 = false;
            panel2.SetActive(false);
            if (dialogBox2 != null)
            {
                dialogBox2.SetActive(false);
            }
        }
    }

    private void Update()
    {
        if (isInRange2 && Input.GetKeyDown(interactKey2))
        {
            panel2.SetActive(false);
            if (dialogBox2 != null)
            {
                dialogBox2.SetActive(true);
            }
        }
    }
}