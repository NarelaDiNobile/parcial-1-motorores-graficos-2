using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teletransportar4 : MonoBehaviour
{
    public GameObject Player4;
    public GameObject TeleportTo4;
    public GameObject StartTeleporter4;

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Teleporter4"))
        {
            Player4.transform.position = TeleportTo4.transform.position;
        }
    }
}

