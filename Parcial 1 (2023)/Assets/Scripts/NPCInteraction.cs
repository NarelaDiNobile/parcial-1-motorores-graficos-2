using UnityEngine;
using UnityEngine.UI;

public class NPCInteraction : MonoBehaviour
{
    public GameObject panel;
    public Text interactText;
    public KeyCode interactKey = KeyCode.E;
    public GameObject dialogBox;
    private bool isInRange = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange = true;
            panel.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange = false;
            panel.SetActive(false);
            if (dialogBox != null)
            {
                dialogBox.SetActive(false);
            }
        }
    }

    private void Update()
    {
        if (isInRange && Input.GetKeyDown(interactKey))
        {
            panel.SetActive(false);
            if (dialogBox != null)
            {
                dialogBox.SetActive(true);
            }
        }
    }
}