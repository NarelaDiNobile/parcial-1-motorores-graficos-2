using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager5 : MonoBehaviour
{
    public GameObject dialoguePanel5;
    public TMP_Text dialogueText5;

    private bool isInRange5 = false;
    private bool hasTriggeredDialogue5 = false;

    void Start()
    {
        dialoguePanel5.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange5 = true;
            dialoguePanel5.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange5 = false;
            dialoguePanel5.SetActive(false);
        }
    }

    void Update()
    {
        if (isInRange5 && Input.GetKeyDown(KeyCode.Space) && !hasTriggeredDialogue5)
        {
            StartDialogue();
            hasTriggeredDialogue5 = true;
        }
    }

    void StartDialogue()
    {
        // Aqu� puedes implementar tu l�gica de di�logo.
        // Puedes usar un sistema de di�logo existente o crear uno personalizado.

        // Ejemplo b�sico:
        dialogueText5.text = "�Hola! Soy el NPC. �Bienvenido!";
    }
}