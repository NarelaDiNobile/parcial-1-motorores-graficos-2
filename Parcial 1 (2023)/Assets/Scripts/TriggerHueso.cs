using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerHueso : MonoBehaviour
{
    public Animator Hueso;
    public Animator Casa;
    public Animator Bowl;

    private void OnTriggerEnter(Collider other)
    {
        Hueso.Play("AnimarHueso");
        Casa.Play("AnimarCasa");
        Bowl.Play("AnimarBowl");
    }

    private void OnTriggerExit(Collider other)
    {
        Hueso.Play("AnimarHueso");
        Casa.Play("AnimarCasa");
        Bowl.Play("AnimarBowl");
    }
}
