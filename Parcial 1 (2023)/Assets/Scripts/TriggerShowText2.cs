using UnityEngine;
using TMPro;

public class TriggerShowText2 : MonoBehaviour
{
    public TextMeshPro textMeshPro2; // Referencia al componente TextMeshPro donde se mostrar� el texto
    public string fullText2 = "When the evening fallsAnd I'm left there with my thoughts";
    public float letterDelay2 = 0.1f; // Retraso entre la aparici�n de cada letra
    public float fadeOutDuration2 = 1f; // Duraci�n de la desaparici�n gradual del texto

    private bool isShowingText2 = false;
    private int currentLetterIndex2 = 0;

    private void Start()
    {
        if (textMeshPro2 == null)
        {
            Debug.LogError("No se ha asignado un componente TextMeshPro en el script TriggerShowText.");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) // Verifica si el objeto que cruz� el trigger tiene la etiqueta "Player"
        {
            isShowingText2 = true;
            textMeshPro2.text = string.Empty; // Limpia el texto actual
            currentLetterIndex2 = 0; // Reinicia el �ndice de letra
            StartCoroutine(ShowTextCoroutine());
        }
    }

    private System.Collections.IEnumerator ShowTextCoroutine()
    {
        while (currentLetterIndex2 < fullText2.Length)
        {
            textMeshPro2.text += fullText2[currentLetterIndex2];
            currentLetterIndex2++;

            yield return new WaitForSeconds(letterDelay2); // Espera el retraso especificado antes de mostrar la siguiente letra
        }

        yield return new WaitForSeconds(fadeOutDuration2); // Espera un tiempo antes de comenzar la desaparici�n gradual del texto
        StartCoroutine(FadeOutTextCoroutine());
    }

    private System.Collections.IEnumerator FadeOutTextCoroutine()
    {
        float elapsedTime = 0f;
        float startAlpha = textMeshPro2.alpha;

        while (elapsedTime < fadeOutDuration2)
        {
            float newAlpha = Mathf.Lerp(startAlpha, 0f, elapsedTime / fadeOutDuration2);
            textMeshPro2.alpha = newAlpha;

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        textMeshPro2.text = string.Empty; // Limpia el texto
        textMeshPro2.alpha = startAlpha; // Restaura la opacidad original
        isShowingText2 = false;
    }

    private void Update()
    {
        if (isShowingText2 && Input.GetKeyDown(KeyCode.Escape))
        {
            StopShowingText();
        }
    }

    private void StopShowingText()
    {
        StopAllCoroutines();
        textMeshPro2.text = string.Empty;
        textMeshPro2.alpha = 1f; // Restaura la opacidad original
        isShowingText2 = false;
    }
}
