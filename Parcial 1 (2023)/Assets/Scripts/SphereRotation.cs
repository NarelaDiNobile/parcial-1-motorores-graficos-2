using UnityEngine;

public class SphereRotation : MonoBehaviour
{
    public float rotationSpeed = 100f;

    void Update()
    {
        // Obtener la rotaci�n actual de la esfera
        Quaternion currentRotation = transform.rotation;

        // Calcular la rotaci�n adicional en el eje Z
        float additionalRotation = rotationSpeed * Time.deltaTime;

        // Aplicar la rotaci�n adicional en el eje Z
        Quaternion newRotation = Quaternion.Euler(currentRotation.eulerAngles.x, currentRotation.eulerAngles.y, currentRotation.eulerAngles.z + additionalRotation);

        // Establecer la nueva rotaci�n de la esfera
        transform.rotation = newRotation;
    }
}