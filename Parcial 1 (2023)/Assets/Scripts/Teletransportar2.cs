using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teletransportar2 : MonoBehaviour
{
    public GameObject Player2;
    public GameObject TeleportTo2;
    public GameObject StartTeleporter2;

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Teleporter2"))
        {
            Player2.transform.position = TeleportTo2.transform.position;
        }
    }
}

