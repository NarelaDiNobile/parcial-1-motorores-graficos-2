using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager9 : MonoBehaviour
{
    public GameObject dialoguePanel9;
    public TMP_Text dialogueText9;

    private bool isInRange9 = false;
    private bool hasTriggeredDialogue9 = false;

    void Start()
    {
        dialoguePanel9.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange9 = true;
            dialoguePanel9.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange9 = false;
            dialoguePanel9.SetActive(false);
        }
    }

    void Update()
    {
        if (isInRange9 && Input.GetKeyDown(KeyCode.Space) && !hasTriggeredDialogue9)
        {
            StartDialogue();
            hasTriggeredDialogue9 = true;
        }
    }

    void StartDialogue()
    {
        // Aqu� puedes implementar tu l�gica de di�logo.
        // Puedes usar un sistema de di�logo existente o crear uno personalizado.

        // Ejemplo b�sico:
        dialogueText9.text = "�Hola! Soy el NPC. �Bienvenido!";
    }
}