using UnityEngine;
using TMPro;

public class TriggerShowText4 : MonoBehaviour
{
    public TextMeshPro textMeshPro4; // Referencia al componente TextMeshPro donde se mostrar� el texto
    public string fullText4 = "But we run our course, we pretend that we're okay, Now if we jump together at least we can swim, Far away from the wreck we made";
    public float letterDelay4 = 0.1f; // Retraso entre la aparici�n de cada letra
    public float fadeOutDuration4 = 1f; // Duraci�n de la desaparici�n gradual del texto

    private bool isShowingText4 = false;
    private int currentLetterIndex4 = 0;

    private void Start()
    {
        if (textMeshPro4 == null)
        {
            Debug.LogError("No se ha asignado un componente TextMeshPro en el script TriggerShowText.");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) // Verifica si el objeto que cruz� el trigger tiene la etiqueta "Player"
        {
            isShowingText4 = true;
            textMeshPro4.text = string.Empty; // Limpia el texto actual
            currentLetterIndex4 = 0; // Reinicia el �ndice de letra
            StartCoroutine(ShowTextCoroutine());
        }
    }

    private System.Collections.IEnumerator ShowTextCoroutine()
    {
        while (currentLetterIndex4 < fullText4.Length)
        {
            textMeshPro4.text += fullText4[currentLetterIndex4];
            currentLetterIndex4++;

            yield return new WaitForSeconds(letterDelay4); // Espera el retraso especificado antes de mostrar la siguiente letra
        }

        yield return new WaitForSeconds(fadeOutDuration4); // Espera un tiempo antes de comenzar la desaparici�n gradual del texto
        StartCoroutine(FadeOutTextCoroutine());
    }

    private System.Collections.IEnumerator FadeOutTextCoroutine()
    {
        float elapsedTime = 0f;
        float startAlpha = textMeshPro4.alpha;

        while (elapsedTime < fadeOutDuration4)
        {
            float newAlpha = Mathf.Lerp(startAlpha, 0f, elapsedTime / fadeOutDuration4);
            textMeshPro4.alpha = newAlpha;

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        textMeshPro4.text = string.Empty; // Limpia el texto
        textMeshPro4.alpha = startAlpha; // Restaura la opacidad original
        isShowingText4 = false;
    }

    private void Update()
    {
        if (isShowingText4 && Input.GetKeyDown(KeyCode.Escape))
        {
            StopShowingText();
        }
    }

    private void StopShowingText()
    {
        StopAllCoroutines();
        textMeshPro4.text = string.Empty;
        textMeshPro4.alpha = 1f; // Restaura la opacidad original
        isShowingText4 = false;
    }
}
