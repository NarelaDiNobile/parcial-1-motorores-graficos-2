using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager3 : MonoBehaviour
{
    public GameObject dialoguePanel3;
    public TMP_Text dialogueText3;

    private bool isInRange3 = false;
    private bool hasTriggeredDialogue3 = false;

    void Start()
    {
        dialoguePanel3.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange3 = true;
            dialoguePanel3.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange3 = false;
            dialoguePanel3.SetActive(false);
        }
    }

    void Update()
    {
        if (isInRange3 && Input.GetKeyDown(KeyCode.Space) && !hasTriggeredDialogue3)
        {
            StartDialogue();
            hasTriggeredDialogue3 = true;
        }
    }

    void StartDialogue()
    {
        // Aqu� puedes implementar tu l�gica de di�logo.
        // Puedes usar un sistema de di�logo existente o crear uno personalizado.

        // Ejemplo b�sico:
        dialogueText3.text = "�Hola! Soy el NPC. �Bienvenido!";
    }
}