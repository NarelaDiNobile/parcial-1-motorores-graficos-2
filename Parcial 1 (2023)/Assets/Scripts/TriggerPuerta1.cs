using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPuerta1 : MonoBehaviour
{
    public Animator Puerta1;

    private void OnTriggerEnter(Collider other)
    {
        Puerta1.Play("Abrir puerta");
    }

    private void OnTriggerExit(Collider other)
    {
        Puerta1.Play("Cerrar Puerta");
    }
}
