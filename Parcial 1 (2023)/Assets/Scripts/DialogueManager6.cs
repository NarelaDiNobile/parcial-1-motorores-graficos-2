using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager6 : MonoBehaviour
{
    public GameObject dialoguePanel6;
    public TMP_Text dialogueText6;

    private bool isInRange6 = false;
    private bool hasTriggeredDialogue6 = false;

    void Start()
    {
        dialoguePanel6.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange6 = true;
            dialoguePanel6.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange6 = false;
            dialoguePanel6.SetActive(false);
        }
    }

    void Update()
    {
        if (isInRange6 && Input.GetKeyDown(KeyCode.Space) && !hasTriggeredDialogue6)
        {
            StartDialogue();
            hasTriggeredDialogue6 = true;
        }
    }

    void StartDialogue()
    {
        // Aqu� puedes implementar tu l�gica de di�logo.
        // Puedes usar un sistema de di�logo existente o crear uno personalizado.

        // Ejemplo b�sico:
        dialogueText6.text = "�Hola! Soy el NPC. �Bienvenido!";
    }
}