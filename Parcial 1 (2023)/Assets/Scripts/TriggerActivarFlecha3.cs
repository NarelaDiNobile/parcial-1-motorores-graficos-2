using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerActivarFlecha3 : MonoBehaviour
{
    public GameObject objectToShowHide3;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            objectToShowHide3.SetActive(true);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            objectToShowHide3.SetActive(false);
        }
    }
}
