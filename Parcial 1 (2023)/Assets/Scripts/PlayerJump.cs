using UnityEngine;

public class PlayerJump : MonoBehaviour
{
    public float fuerzaSalto = 20f;
    public int cantSaltosDisponibles = 2;

    private int saltosRealizados = 0;
    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && saltosRealizados < cantSaltosDisponibles)
        {
            saltosRealizados++;
            rb.velocity = new Vector3(rb.velocity.x, 0f, rb.velocity.z); // Resetear la velocidad vertical antes del salto doble
            rb.AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            saltosRealizados = 0; // Restablecer el contador de saltos cuando el jugador toca el suelo
        }
    }
}
