using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager2 : MonoBehaviour
{
    public GameObject dialoguePanel2;
    public TMP_Text dialogueText2;

    private bool isInRange2 = false;
    private bool hasTriggeredDialogue2 = false;

    void Start()
    {
        dialoguePanel2.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange2 = true;
            dialoguePanel2.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange2 = false;
            dialoguePanel2.SetActive(false);
        }
    }

    void Update()
    {
        if (isInRange2 && Input.GetKeyDown(KeyCode.Space) && !hasTriggeredDialogue2)
        {
            StartDialogue();
            hasTriggeredDialogue2 = true;
        }
    }

    void StartDialogue()
    {
        // Aqu� puedes implementar tu l�gica de di�logo.
        // Puedes usar un sistema de di�logo existente o crear uno personalizado.

        // Ejemplo b�sico:
        dialogueText2.text = "�Hola! Soy el NPC. �Bienvenido!";
    }
}