using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PanelTexto2 : MonoBehaviour
{
    public GameObject panelTexto2;
    public TMP_Text texto2;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Mostrar el panel de texto
            panelTexto2.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Ocultar el panel de texto
            panelTexto2.SetActive(false);
        }
    }
}
