using System;
using UnityEngine;

public class PowerUpManager : MonoBehaviour
{
    public bool isPoweredUp = false;
    public float powerUpDuration = 5f;

    private float powerUpTimer = 0f;

    public void ActivatePowerUp()
    {
        isPoweredUp = true;
        powerUpTimer = powerUpDuration;
    }

    private void Update()
    {
        if (isPoweredUp)
        {
            powerUpTimer -= Time.deltaTime;
            if (powerUpTimer <= 0f)
            {
                isPoweredUp = false;
                powerUpTimer = 0f;
            }
        }
    }
}

 
