using UnityEngine;

public class TriggerFadeOutMusica : MonoBehaviour
{
    public AudioSource musicAudioSource; // Referencia al componente AudioSource de la m�sica
    public float fadeDuration = 1f; // Duraci�n de la transici�n de volumen

    private bool hasCrossedTrigger = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) // Verifica si el objeto que cruz� el trigger tiene la etiqueta "Player"
        {
            if (!hasCrossedTrigger)
            {
                hasCrossedTrigger = true;
                StartCoroutine(FadeOutMusic());
            }
        }
    }

    private System.Collections.IEnumerator FadeOutMusic()
    {
        float elapsedTime = 0f;
        float startVolume = musicAudioSource.volume;

        while (elapsedTime < fadeDuration)
        {
            float newVolume = Mathf.Lerp(startVolume, 0f, elapsedTime / fadeDuration);
            musicAudioSource.volume = newVolume;

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        musicAudioSource.Stop();
        musicAudioSource.volume = startVolume; // Restaura el volumen original
    }
}
