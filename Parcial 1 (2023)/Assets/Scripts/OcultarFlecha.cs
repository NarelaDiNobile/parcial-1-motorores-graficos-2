using UnityEngine;

public class OcultarFlecha : MonoBehaviour
{
    public GameObject arrow3;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            arrow3.SetActive(false);
        }
    }
}
