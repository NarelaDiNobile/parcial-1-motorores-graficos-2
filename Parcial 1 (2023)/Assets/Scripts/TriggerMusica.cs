using UnityEngine;

public class TriggerMusica : MonoBehaviour
{
    public AudioClip musicClip; // Referencia al AudioClip de la canci�n que deseas reproducir

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) // Verifica si el objeto que cruz� el trigger tiene la etiqueta "Player"
        {
            AudioSource audioSource = GetComponent<AudioSource>();

            if (audioSource == null)
            {
                audioSource = gameObject.AddComponent<AudioSource>();
            }

            audioSource.clip = musicClip;
            audioSource.Play();
        }
    }
}