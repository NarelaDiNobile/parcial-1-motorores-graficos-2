using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager : MonoBehaviour
{
    public GameObject dialoguePanel;
    public TMP_Text dialogueText;
    public AudioSource dialogueAudioSource;

    private bool isInRange = false;
    private bool hasTriggeredDialogue = false;

    void Start()
    {
        dialoguePanel.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange = true;
            dialoguePanel.SetActive(true);
            dialogueAudioSource.Play();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange = false;
            dialoguePanel.SetActive(false);
            dialogueAudioSource.Stop();
        }
    }

    void Update()
    {
        if (isInRange && Input.GetKeyDown(KeyCode.Space) && !hasTriggeredDialogue)
        {
            StartDialogue();
            hasTriggeredDialogue = true;
        }
    }

    void StartDialogue()
    {
        // Aqu� puedes implementar tu l�gica de di�logo.
        // Puedes usar un sistema de di�logo existente o crear uno personalizado.

        // Ejemplo b�sico:
        dialogueText.text = "�Hola! Soy el NPC. �Bienvenido!";
    }
}