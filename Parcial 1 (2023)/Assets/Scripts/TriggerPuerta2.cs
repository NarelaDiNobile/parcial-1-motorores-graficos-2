using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPuerta2 : MonoBehaviour
{
    public Animator Puerta2;

    private void OnTriggerEnter(Collider other)
    {
        Puerta2.Play("AbrirPuerta2");
    }

    private void OnTriggerExit(Collider other)
    {
        Puerta2.Play("CerrarPuerta2");
    }
}
