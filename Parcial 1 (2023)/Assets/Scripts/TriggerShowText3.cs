using UnityEngine;
using TMPro;

public class TriggerShowText3 : MonoBehaviour
{
    public TextMeshPro textMeshPro3; // Referencia al componente TextMeshPro donde se mostrar� el texto
    public string fullText3 = "And the image of you being with someone else, Well, that's eating me up inside";
    public float letterDelay3 = 0.1f; // Retraso entre la aparici�n de cada letra
    public float fadeOutDuration3 = 1f; // Duraci�n de la desaparici�n gradual del texto

    private bool isShowingText3 = false;
    private int currentLetterIndex3 = 0;

    private void Start()
    {
        if (textMeshPro3 == null)
        {
            Debug.LogError("No se ha asignado un componente TextMeshPro en el script TriggerShowText.");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) // Verifica si el objeto que cruz� el trigger tiene la etiqueta "Player"
        {
            isShowingText3 = true;
            textMeshPro3.text = string.Empty; // Limpia el texto actual
            currentLetterIndex3 = 0; // Reinicia el �ndice de letra
            StartCoroutine(ShowTextCoroutine());
        }
    }

    private System.Collections.IEnumerator ShowTextCoroutine()
    {
        while (currentLetterIndex3 < fullText3.Length)
        {
            textMeshPro3.text += fullText3[currentLetterIndex3];
            currentLetterIndex3++;

            yield return new WaitForSeconds(letterDelay3); // Espera el retraso especificado antes de mostrar la siguiente letra
        }

        yield return new WaitForSeconds(fadeOutDuration3); // Espera un tiempo antes de comenzar la desaparici�n gradual del texto
        StartCoroutine(FadeOutTextCoroutine());
    }

    private System.Collections.IEnumerator FadeOutTextCoroutine()
    {
        float elapsedTime = 0f;
        float startAlpha = textMeshPro3.alpha;

        while (elapsedTime < fadeOutDuration3)
        {
            float newAlpha = Mathf.Lerp(startAlpha, 0f, elapsedTime / fadeOutDuration3);
            textMeshPro3.alpha = newAlpha;

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        textMeshPro3.text = string.Empty; // Limpia el texto
        textMeshPro3.alpha = startAlpha; // Restaura la opacidad original
        isShowingText3 = false;
    }

    private void Update()
    {
        if (isShowingText3 && Input.GetKeyDown(KeyCode.Escape))
        {
            StopShowingText();
        }
    }

    private void StopShowingText()
    {
        StopAllCoroutines();
        textMeshPro3.text = string.Empty;
        textMeshPro3.alpha = 1f; // Restaura la opacidad original
        isShowingText3 = false;
    }
}
