using UnityEngine;
using TMPro;

public class TriggerShowText : MonoBehaviour
{
    public TextMeshPro textMeshPro; // Referencia al componente TextMeshPro donde se mostrar� el texto
    public string fullText = "Lately, I've been, I've been thinking\nI want you to be happier, I want you to be happier";
    public float letterDelay = 0.1f; // Retraso entre la aparici�n de cada letra
    public float fadeOutDuration = 1f; // Duraci�n de la desaparici�n gradual del texto

    private bool isShowingText = false;
    private int currentLetterIndex = 0;

    private void Start()
    {
        if (textMeshPro == null)
        {
            Debug.LogError("No se ha asignado un componente TextMeshPro en el script TriggerShowText.");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) // Verifica si el objeto que cruz� el trigger tiene la etiqueta "Player"
        {
            isShowingText = true;
            textMeshPro.text = string.Empty; // Limpia el texto actual
            currentLetterIndex = 0; // Reinicia el �ndice de letra
            StartCoroutine(ShowTextCoroutine());
        }
    }

    private System.Collections.IEnumerator ShowTextCoroutine()
    {
        while (currentLetterIndex < fullText.Length)
        {
            textMeshPro.text += fullText[currentLetterIndex];
            currentLetterIndex++;

            yield return new WaitForSeconds(letterDelay); // Espera el retraso especificado antes de mostrar la siguiente letra
        }

        yield return new WaitForSeconds(fadeOutDuration); // Espera un tiempo antes de comenzar la desaparici�n gradual del texto
        StartCoroutine(FadeOutTextCoroutine());
    }

    private System.Collections.IEnumerator FadeOutTextCoroutine()
    {
        float elapsedTime = 0f;
        float startAlpha = textMeshPro.alpha;

        while (elapsedTime < fadeOutDuration)
        {
            float newAlpha = Mathf.Lerp(startAlpha, 0f, elapsedTime / fadeOutDuration);
            textMeshPro.alpha = newAlpha;

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        textMeshPro.text = string.Empty; // Limpia el texto
        textMeshPro.alpha = startAlpha; // Restaura la opacidad original
        isShowingText = false;
    }

    private void Update()
    {
        if (isShowingText && Input.GetKeyDown(KeyCode.Escape))
        {
            StopShowingText();
        }
    }

    private void StopShowingText()
    {
        StopAllCoroutines();
        textMeshPro.text = string.Empty;
        textMeshPro.alpha = 1f; // Restaura la opacidad original
        isShowingText = false;
    }
}

