using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teletransportar3 : MonoBehaviour
{
    public GameObject Player3;
    public GameObject TeleportTo3;
    public GameObject StartTeleporter3;

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Teleporter3"))
        {
            Player3.transform.position = TeleportTo3.transform.position;
        }
    }
}

