using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PanelTexto : MonoBehaviour
{
    public GameObject panelTexto;
    public TMP_Text texto;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Mostrar el panel de texto
            panelTexto.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Ocultar el panel de texto
            panelTexto.SetActive(false);
        }
    }
}
