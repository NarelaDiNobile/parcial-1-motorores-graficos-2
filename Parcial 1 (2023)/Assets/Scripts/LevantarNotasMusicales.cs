using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevantarNotasMusicales : MonoBehaviour
{
    public AudioClip sonidoColision;
    private ContadorNotasMusicales scriptContarNotas;

    void Start()
    {
        scriptContarNotas = FindObjectOfType<ContadorNotasMusicales>();
    }

    private void OnTriggerEnter(Collider other)
    {
        this.gameObject.SetActive(false);
        scriptContarNotas.contador_monedas += 1;
        AudioSource.PlayClipAtPoint(sonidoColision, transform.position);
    }
}
