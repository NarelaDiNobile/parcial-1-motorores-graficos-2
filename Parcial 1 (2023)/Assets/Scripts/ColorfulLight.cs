using UnityEngine;

public class ColorfulLight : MonoBehaviour
{
    public Color[] colors;
    public float colorChangeInterval = 1f;
    public float movementDistance = 1f;
    public float movementSpeed = 1f;

    private Light lightComponent;
    private int currentColorIndex = 0;
    private Vector3 initialPosition;
    private Vector3 targetPosition;
    private float t = 0f;

    void Start()
    {
        lightComponent = GetComponent<Light>();
        initialPosition = transform.position;
        targetPosition = initialPosition + Vector3.up * movementDistance;

        InvokeRepeating("ChangeColor", 0f, colorChangeInterval);
    }

    void Update()
    {
        // Movimiento periódico entre dos posiciones
        t += Time.deltaTime * movementSpeed;
        transform.position = Vector3.Lerp(initialPosition, targetPosition, Mathf.PingPong(t, 1f));

        // Cambio de color gradual
        lightComponent.color = Color.Lerp(colors[currentColorIndex], colors[(currentColorIndex + 1) % colors.Length], t % colorChangeInterval / colorChangeInterval);
    }

    void ChangeColor()
    {
        currentColorIndex = (currentColorIndex + 1) % colors.Length;
    }
}