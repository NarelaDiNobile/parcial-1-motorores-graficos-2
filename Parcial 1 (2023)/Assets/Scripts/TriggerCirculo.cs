using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerCirculo : MonoBehaviour
{
    public Animator Circulo;

    private void OnTriggerEnter(Collider other)
    {
        Circulo.Play("AnimarCirculo");
    }
}
