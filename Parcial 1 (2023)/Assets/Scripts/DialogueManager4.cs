using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager4 : MonoBehaviour
{
    public GameObject dialoguePanel4;
    public TMP_Text dialogueText4;

    private bool isInRange4 = false;
    private bool hasTriggeredDialogue4 = false;

    void Start()
    {
        dialoguePanel4.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange4 = true;
            dialoguePanel4.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange4 = false;
            dialoguePanel4.SetActive(false);
        }
    }

    void Update()
    {
        if (isInRange4 && Input.GetKeyDown(KeyCode.Space) && !hasTriggeredDialogue4)
        {
            StartDialogue();
            hasTriggeredDialogue4 = true;
        }
    }

    void StartDialogue()
    {
        // Aqu� puedes implementar tu l�gica de di�logo.
        // Puedes usar un sistema de di�logo existente o crear uno personalizado.

        // Ejemplo b�sico:
        dialogueText4.text = "�Hola! Soy el NPC. �Bienvenido!";
    }
}