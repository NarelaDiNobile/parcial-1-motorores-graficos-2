using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPuerta3 : MonoBehaviour
{
    public Animator Puerta3;

    private void OnTriggerEnter(Collider other)
    {
        Puerta3.Play("AbrirPuerta3");
    }

    private void OnTriggerExit(Collider other)
    {
        Puerta3.Play("CerrarPuerta3");
    }
}
