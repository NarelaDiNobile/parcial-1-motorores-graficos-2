using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager7 : MonoBehaviour
{
    public GameObject dialoguePanel7;
    public TMP_Text dialogueText7;

    private bool isInRange7 = false;
    private bool hasTriggeredDialogue7 = false;

    void Start()
    {
        dialoguePanel7.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange7 = true;
            dialoguePanel7.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInRange7 = false;
            dialoguePanel7.SetActive(false);
        }
    }

    void Update()
    {
        if (isInRange7 && Input.GetKeyDown(KeyCode.Space) && !hasTriggeredDialogue7)
        {
            StartDialogue();
            hasTriggeredDialogue7 = true;
        }
    }

    void StartDialogue()
    {
        // Aqu� puedes implementar tu l�gica de di�logo.
        // Puedes usar un sistema de di�logo existente o crear uno personalizado.

        // Ejemplo b�sico:
        dialogueText7.text = "�Hola! Soy el NPC. �Bienvenido!";
    }
}